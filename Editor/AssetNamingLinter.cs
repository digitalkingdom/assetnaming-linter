using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;

namespace AssetNames.Editor
{
    public static class AssetNamingLinter
    {
        [MenuItem("Assets/Tools/Lint asset names")]
        public static void LintAssetNames()
        {
            var report = new FilenameReport();

            var allFolders = GetAllFolders("Assets");

            // Inspect all asset names in a set of folders, ensure they match our naming convention
            foreach (var assetGuid in AssetDatabase.FindAssets("", AssetNamingConfig.Config.folders))
            {
                var assetPath = AssetDatabase.GUIDToAssetPath(assetGuid);

                // Subfolders are listed as assets of type: DefaultAsset. DefaultAsset is used for assets
                // that do not have a specific type (yet). This means that AssetDatabase.FindAssets can't filter
                // subfolders and they will be listed when iterating through all assets. Since the type can not
                // be used to identify which assets are folders and which aren't, use a list of all subfolders
                // to identify folders and discard them.
                if (!allFolders.Contains(assetPath))
                {

                    var assetTypeName = AssetDatabase.GetMainAssetTypeAtPath(assetPath).Name.ToLowerInvariant();

                    // Case-insensitive asset type filter
                    var isTypeExcluded = AssetNamingConfig.Config.excludedTypes.Any(fileType =>
                        fileType.ToLowerInvariant().Equals(assetTypeName));

                    if (isTypeExcluded)
                    {
                        continue;
                    }

                    var assetName = Path.GetFileNameWithoutExtension(assetPath);


                    if (!Regex.IsMatch(assetName, AssetNamingConfig.Config.namingConventionRegex))
                    {
                        report.incorrectName.Add(assetPath);
                    }
                    else if (AssetNamingConfig.Config.prefixes.All(p =>
                                 !assetName.StartsWith(p) && // The file either starts with the prefix
                                 !assetName.Equals(p.Substring(0,
                                     p.Length - 1)))) // Or it is the prefix (minus the underscore)
                    {
                        report.incorrectPrefix.Add(assetPath);
                    }
                    else
                    {
                        report.correctFiles.Add(assetPath);
                    }
                }
            }

            PrintReport(report);

            if (report.incorrectName.Count > 0 || report.incorrectPrefix.Count > 0)
            {
                throw new Exception("Some assets are incorrectly named. Check the report above to see which.");
            }
        }

        private static List<string> GetAllFolders(string baseFolder)
        {
            var folders = AssetDatabase.GetSubFolders(baseFolder);

            var allFolders = new List<string>();

            foreach (var folder in folders)
            {
                allFolders.Add(folder);
                allFolders.AddRange(GetAllFolders(folder));
            }

            return allFolders;
        }

        private static void PrintReportByBatches(List<string> names, string textWhenEmpty, string batchTitle)
        {
            /* Using a string builder will force unity console to print everything at once, which seems to fails in case of big strings with the following error.
             * ArgumentOutOfRangeException: Index and length must refer to a location within the string.
             * Parameter name: length
             * System.String.Substring (System.Int32 startIndex, System.Int32 length) (at <695d1cc93cca45069c528c15c9fdd749>:0)
             */
            //  After testing, it seems that it is neither a fixed max line not a fixed character lines. I could not find an exact reason fo the error, so we will simply print a new line every x lines, which seems fine

            var sb = new StringBuilder();
            const int nbBatchLines = 50;
            var currentNameBatchIndex = 1;
            var nbNameCache = 0;
            var totalNameBatches = Mathf.CeilToInt(names.Count / (float)nbBatchLines);

            sb.AppendLine("Asset names report");
            sb.AppendLine(names.Count == 0
                ? textWhenEmpty
                : $"{batchTitle} ({currentNameBatchIndex}/{totalNameBatches}):");
            currentNameBatchIndex++;

            foreach (var name in names)
            {
                sb.AppendLine($"- {name}");
                nbNameCache++;

                if (nbNameCache % nbBatchLines == 0)
                {
                    nbNameCache = 0;
                    sb.Dump();
                    sb.AppendLine("Asset names report");
                    sb.AppendLine($"{batchTitle} ({currentNameBatchIndex}/{totalNameBatches}):");
                    currentNameBatchIndex++;
                }
            }

            sb.Dump();
        }

        private static void PrintReport(FilenameReport report)
        {
            PrintReportByBatches(report.incorrectName, "No incorrect naming!", "Incorrect Naming");
            PrintReportByBatches(report.incorrectPrefix, "No incorrect prefixes!", "Incorrect Prefixes");
            PrintReportByBatches(report.correctFiles, "No correct naming!", "Correct Naming");
        }

        private static void Dump(this StringBuilder stringBuilder)
        {
            Debug.Log(stringBuilder.ToString());
            stringBuilder.Clear();
        }

        private class FilenameReport
        {
            public readonly List<string> correctFiles = new List<string>();
            public readonly List<string> incorrectName = new List<string>();
            public readonly List<string> incorrectPrefix = new List<string>();
        }
    }
}