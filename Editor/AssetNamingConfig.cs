using UnityEditor;
using UnityEngine;

namespace AssetNames.Editor
{
    public class AssetNamingConfig : ScriptableObject
    {
        [Tooltip("List of valid prefixes. Any linted asset must start with one of these.")]
        public string[] prefixes = {""};

        [Tooltip("Regular expression used to parse names. Linted assets must match this.")]
        public string namingConventionRegex = "[A-Z][a-zA-Z]+(_[A-Z][a-zA-Z]+)*";

        [Tooltip("List of folders containing assets to lint. Relative to the project root directory.")]
        public string[] folders =
        {
            "Assets/Animations",
            "Assets/Materials",
            "Assets/Models",
            "Assets/Prefabs",
            "Assets/Settings",
            "Assets/Shaders",
            "Assets/Sprites",
            "Assets/Textures",
            "Assets/Videos"
        };

        [Tooltip("Types to exclude (case-insensitive).")]
        public string[] excludedTypes = {};

        public static AssetNamingConfig Config
        {
            get
            {
                var instance = Resources.Load<AssetNamingConfig>("AssetNamingConfig");
                if (instance == null)
                {
                    instance = CreateInstance<AssetNamingConfig>();
                    AssetDatabase.CreateAsset (instance, $"Assets/Resources/AssetNamingConfig.asset");
                }

                return instance;
            }
        }
    }
}