# Asset names Linter

Installs under `Assets/Tools/Lint asset names`.
This will scan all configured folders and all assets within then and compare their names with the given naming convention.

You can use this in the CI by running the following command line:

```bash
UNITY_EXECUTABLE -batchmode -quit -nographics -executemethod AssetNames.Editor.AssetNamingLinter.LintAssetNames
```

A log will be printed listing all the linted assets, and their state (wrong/ok).
An exception will be raised if any misnamed asset was found.

Upon first running the tool, an asset will be created under `Assets/Resources/AssetNamingConfig.asset` which contains the tool's config.
